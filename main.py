#!/usr/bin/python3

def say_hello(name: str):
    print(f"Hello, {name}")


if __name__ == "__main__":
    say_hello("World")
